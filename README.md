# Dotfiles

A collection of dotfiles for my development setup. I love good tools!


Core Tools:

| Tool     | Description                     | Purpose                                               |
|:---------|:--------------------------------|:------------------------------------------------------|
| ghostty  | Terminal emulator               | Fast, GPU-accelerated terminal experience             |
| gitui    | Terminal-based Git interface    | Intuitive Git operations without leaving the terminal |
| helix    | Terminal-based IDE              | Powerful editing with modern features built-in        |
| neovim   | Enhanced Vim editor             | Extensible editing with Vim philosophy                |
| nushell  | Structured data-oriented shell  | Modern shell with data processing capabilities        |
| starship | Minimal, informative prompt     | Clean, informative command prompt                     |
| yazi     | File manager                    | Lightning-fast file operations                        |
| mise     | Development environment manager | Seamless runtime version management                   |


## Shell Setup


The combination of two shells provides both system efficiency and user convenience.
I use `dash` as a login shell. This is the "unerlying" shell my environment uses.
For interactive usage I like to use a more human-friendly shell. Nushell is exactly that.

- Login Shell: dash
  - POSIX compliant
  - Optimized for performance
  - Handles system initialization
- Interactive Shell: nushell
  - Rich data processing capabilities
  - User-friendly interface
  - Modern shell features

As a shell prompt for my interactive shell I use `starship`, which is easy to
configure and is highly configurable. With my config I tried to minimize the
visual footprint of the prompt and keep it simple. The most complex feature I
use is a git status in the prompt.

![Screenshot placeholder: nushell configuration]()

# Git Workflow

Git can alias shell scripts. The use of nushell for development makes it easy
to enhance git to my personal needs.

| Alias  | Function                                                    |
|:-------|:------------------------------------------------------------|
| git bb | Enhanced branch viewing (git-better-branch.nu)              |
| git cc | Interactive conventional commit prompt (git-conv-commit.nu) |
| git lg | Improved log visualization                                  |

![Screenshot placeholder: git-better-branch output]()

## Advanced Git Features

I love git worktrees, as I really complements todays workflows. Being able to
checkout multiple branches at the same time, and not dealing with stash is
very nice. In combination with bare repos I developt a pretty good workflow I'm
very efficient in.

![Screenshot placeholder: bare repository structure]()

