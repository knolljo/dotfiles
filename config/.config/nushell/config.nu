# Nushell Config File
#
# version = "0.101.0"
 
alias ja = yazi
alias tree = eza --tree
alias ll = eza -lagoih

$env.config.show_banner = false
$env.config.rm.always_trash = true

$env.config.edit_mode = "vi"
$env.config.cursor_shape = {
  # block, underscore, line, blink_block, blink_underscore, blink_line
  vi_insert: line
  vi_normal: block
}

let carapace_completer = {|spans: list<string>|
    carapace $spans.0 nushell ...$spans
    | from json
    | if ($in | default [] | where value =~ '^-.*ERR$' | is-empty) { $in } else { null }
}

let fish_completer = {|spans|
    fish --command $'complete "--do-complete=($spans | str join " ")"'
    | from tsv --flexible --noheaders --no-infer
    | rename value description
}

let zoxide_completer = {|spans|
    $spans | skip 1 | zoxide query -l ...$in | lines | where {|x| $x != $env.PWD}
}

# This completer will use carapace by default
let external_completer = {|spans|
    let expanded_alias = scope aliases
    | where name == $spans.0
    | get -i 0.expansion

    let spans = if $expanded_alias != null {
        $spans
        | skip 1
        | prepend ($expanded_alias | split row ' ' | take 1)
    } else {
        $spans
    }

    match $spans.0 {
        # carapace completions are incorrect for nu
        nu => $fish_completer
        # fish completes commits and branch names in a nicer way
        git => $fish_completer
        # carapace doesn't have completions for asdf
        asdf => $fish_completer
        # use zoxide completions for zoxide commands
        __zoxide_z | __zoxide_zi => $zoxide_completer
        _ => $carapace_completer
    } | do $in $spans
}

$env.config = {
    # ...
    completions: {
        external: {
            enable: true
            completer: $external_completer
        }
    }
    # ...
}


$env.config.keybindings = [
  {
    name: open_command_editor
    modifier: control
    keycode: char_e
    mode: [emacs, vi_normal, vi_insert]
    event: { send: openeditor }
  }
  {
    name: change_dir_with_fzf
    modifier: CONTROL
    keycode: char_f
    mode: vi_insert
    event: {
      send: executehostcommand,
      cmd: "cd (FZF_DEFAULT_COMMAND='fd -td' fzf --height=33 --layout=reverse)"
      }
  }
]

$env.config.color_config = {
  separator: white
  header: red
  empty: blue
  bool: light_cyan
  int: white
  filesize: white
  duration: white
  date: {|| (date now) - $in |
    if $in < 1hr {
      'purple'
    } else if $in < 6hr {
      'red'
    } else if $in < 1day {
      'yellow'
    } else if $in < 3day {
      'green'
    } else if $in < 1wk {
      'light_green'
    } else if $in < 6wk {
      'cyan'
    } else if $in < 52wk {
      'blue'
    } else { 'dark_gray' }
  }
  range: white
  float: green
  string: white
  nothing: white
  binary: white
  cellpath: white
  row_index: green_bold
  record: white
  list: white
  block: white
  hints: dark_gray

  shape_and: purple_bold
  shape_binary: purple_bold
  shape_block: blue_bold
  shape_closure: green_bold
  shape_custom: green
  shape_datetime: cyan_bold
  shape_external: white_bold
  shape_externalarg: green_bold
  shape_filepath: white_bold
  shape_directory: white_bold
  shape_flag: light_cyan
  shape_bool: "#c586c0"
  shape_float: "#b5cea8"
  shape_int: "#b5cea8"
  shape_garbage: { fg: white bg: red attr: b}
  shape_globpattern: cyan_bold
  shape_internalcall: "#c586c0"
  shape_list: cyan_bold
  shape_literal: blue
  shape_match_pattern: green
  shape_matching_brackets: { attr: u }
  shape_nothing: light_cyan
  shape_operator: yellow
  shape_or: purple_bold
  shape_pipe: white_bold
  shape_range: yellow_bold
  shape_record: cyan_bold
  shape_redirection: purple_bold
  shape_signature: green_bold
  shape_string: "#ce9178"
  shape_string_interpolation: "#ce9178"
  shape_table: blue_bold
  shape_variable: "#9cdcfe"
  shape_vardecl: "#9cdcfe"
}

use ($nu.default-config-dir | path join mise.nu)
