def print_clock [] {
  print $"(date now | format date "%H:%M")"
}

let initial_sleep = 60sec - (date now | format date "%Ssec" | into duration)

print_clock
sleep $initial_sleep

loop {
  print_clock
  sleep 60sec
}
