def 'git aicommit' [] {

  print $"(ansi blue_bold)running(ansi reset): git diff of staged area"
  let git_diffs = git  --no-pager diff --staged --minimal --no-color --function-context --no-ext-diff -- :!*.lock  :!*.lockb

  if ($git_diffs | str length) == 0 {
    print $"(ansi yellow_bold)error(ansi reset): nothing in staging area"
    return
  }

  let gemini_in = $"
  Don't explain.
  You will be provided with an output from the `git diff --staged` command. Your task is to construct a clean and comprehensive commit message in present tense for the code changes with the following keys only in JSON format with no additional formatting:
  - type: A label from the following list [feat, fix, docs, style, refactor, perf, test, build, ci, chore] that represents the code changes
  - description: A succinct description of the code changes in a single sentence, without a period at the end

  ($git_diffs)
  "

  let gemini_out = (
    http post
    -t application/json
    $'https://generativelanguage.googleapis.com/v1beta/models/gemini-1.5-flash-latest:generateContent?key=($env.API_KEY)'
    {"contents":[{"parts":[{"text":$gemini_in}]}]}
  )

  let msg = $gemini_out | get candidates.content.parts.0.text
  let json = $msg | parse "```json\n{text}\n```"
  let commit = $json | get 0.text | from json

  print $"(ansi green_bold)($commit.type): (ansi white_bold)($commit.description)(ansi reset)"
}
