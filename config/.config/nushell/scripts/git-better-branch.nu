#!/usr/bin/env nu

def count_commits [
    base_branch: string
    branch: string
] {
    git rev-list --left-right --count $"($base_branch)...($branch)" | parse "{ahead}\t{behind}" | get 0
}


# Show branches in a pretty format
def git-better-branch [] {
    let main_branch = (git rev-parse HEAD)
    const format_string = "%(objectname:short)@%(refname:short)@%(committerdate:relative)"
    (
        git for-each-ref --sort=-authordate --format $format_string refs/heads/
        | parse "{sha}@{name}@{time}"
        | each { |branch|
            let commits = count_commits $branch.sha $main_branch
            {
                $"(ansi light_green)Ahead": $"(ansi light_green)($commits.ahead)",
                $"(ansi purple)Behind": $"(ansi purple)($commits.behind)",
                $"(ansi light_blue)Branch": $"(ansi light_blue)($branch.name)",
                $"(ansi xterm_tan)Last Commit": $"(ansi xterm_tan)($branch.time)",
            }
        }
        | table -i false
    )
}

export def main [] {
    git-better-branch
}
