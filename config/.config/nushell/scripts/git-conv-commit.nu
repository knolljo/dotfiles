#!/usr/bin/env nu

# 0. Check if in repo && Check for staged
# 1. Commit type
# 2. Breaking changes
# 3. Scope
# 4. SHORT, IMPERATIVE message
# 5. LONG, WHAT & WHY message
# 6. Closes/links issues

const verbs = {
    feat: [ "add", "implement", "introduce", "create", "enable", "support", "integrate", ],
    fix: [ "resolve", "correct", "update", "patch", "address", "eliminate", "handle", ],
    refactor: [ "simplify", "restructure", "reorganize", "optimize", "improve", "extract", "consolidate", ],
    docs: [ "update", "add", "revise", "clarify", "document", "improve", "expand", ],
    style: [ "format", "align", "standardize", "clean", "normalize", "adjust", "reformat", ],
    test: [ "add" "update" "improve" "extend" "enhance" "implement" "refactor"         ],
    chore: [ "update", "upgrade", "bump", "maintain", "clean", "remove", "configure",         ],
    build: [ "update", "modify", "configure", "optimize", "setup", "integrate", "automate", ],
    ci: [ "configure", "setup", "update", "implement", "automate", "optimize", "add", ],
    perf: [ "optimize", "improve", "enhance", "speed up", "reduce", "accelerate", "streamline", ],
}

const types = [
      [type description emoji];
      ["fix" "A bug fix" "🐛 "]
      ["feat" "A new feature" "✨ "]
      ["docs" "Documentation only changes" "📝 "]
      ["style" "Changes that do not affect the meaning of the code" "🎨 "]
      ["refactor" "A code change that neither fixes a bug nor adds a feature" "♻️ "]
      ["perf" "A code change that improves performance" "⚡️ "]
      ["test" "Adding missing tests or correcting existing tests" "🧪 "]
      ["build" "Changes that affect the build system or external dependencies" "👷‍♂️ "]
      ["ci" "Changes to our CI configuration files and scripts" "🤖 "]
      ["chore" "Other changes that dont modify src or test files" "🧹 "]
      ["revert" "Reverts a previous commit" "↩️ "]
]


def files_to_stage [] {
	git status -s | detect columns --no-headers | where column0 != "A" | get column1 | input list -m
}


def repo_has_staged_changes [] {
	# (git status -s | lines | length) > 0
	(git diff --cached | lines | length) > 0
}


def get_type [] {
	let selected_index = (
        $types
        | format pattern $"(ansi palegreen1a){type}(ansi reset):   \t {description}"
        | input list -if $'(ansi green_bold)Type(ansi reset)'
        | default null
    )

	if $selected_index == null {
		print $"(ansi yb)aborted(ansi reset): no commit type selected"
    exit -1
	}
	
	let selected = $types | get $selected_index
  print $'(ansi bo)(ansi green_bold)Type(ansi reset): ($selected.emoji) (ansi palegreen1a)($selected.type)'
	return $selected

}


def get_scope [] {
	input $"(ansi bo)(ansi xblue)Scope(ansi reset): (ansi aqua)"
}


# A SHORT and IMPERATIVE message
def get_verb [
	type: string
] {
	($verbs | get $type) | input list -f "Select verb: "
}


# A SHORT and IMPERATIVE message
def get_message [
	verb: string = "" # Verb to use as prefix
] {
	input $"(ansi bo)(ansi olive)Message(ansi reset): ($verb) "
}


# Longer description about WHAT & WHY
def get_description [] {
	input $"(ansi bo)(ansi mediumpurple)Description(ansi reset): "
}

# def get_issue_client [] {
# 	let url = (git remote get-url origin)
#   (git remote get-url origin) | split column / | get column3
# 	match ($url) {
# 		"https://github.com" => `gh issues list`,
# 		"https://gitlab.com" => `glab issues list`,
# 		"https://codeberg.org" => `berg issue list`,
# 	}
# }


# Issues affected
def get_issues [] {
	# gh issues list | input list -m "Closes issues:"
	input list "Closes issues:"
}


export def main [
	--emoji  # Enable emoji commit message
] {
	if not (repo_has_staged_changes) {
		print "no files staged"	
		let staged = files_to_stage
		print ($staged | str join " ")
		git add ...$staged
	}

	let type = get_type
	let scope = get_scope
	let verb = get_verb $type.type
	let message = get_message $verb
	# let description = get_description
	# let closes = get_issues

	echo $"(if $emoji {$type.emoji})(ansi xgreen)($type.type)(ansi xblue)(if $scope != "" {$"\(($scope)\)"} else ""): (ansi olive)($verb) ($message)(ansi reset)"
	git commit --edit -m $"(if $emoji {$type.emoji})($type.type)(if $scope != "" {$"\(($scope)\)"} else ""): ($verb) ($message)"
}

