

def main [
  url: string  # Repo url
] {

  let path = $url | path basename | str replace ".git" ""

  mkdir $path
  cd $path

  git clone --bare $url .bare
  echo "gitdir: ./.bare" | save .git
  git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"

}


