def eprint [msg: string] {
  print -e $"(ansi rb)error:(ansi reset) ($msg)"
}

def sprint [msg: string] {
  print $"(ansi gb)success:(ansi reset) ($msg)"
}

def aprint [pre: string, msg: string] {
  print $"(ansi c)($pre):(ansi reset) ($msg)"
}

aprint "pulling" "image ac75d231c1901bfafbc4f85baccefd36b691e842"
sleep 5sec
sprint "pulled image ac75d231c1901bfafbc4f85baccefd36b691e842"
sleep 1sec
eprint "starting container from image ac75d231c1901bfafbc4f85baccefd36b691e842"
