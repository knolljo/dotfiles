def main [
    keyword: string  # Word to count
] {
  (
    rg -c $keyword
    | lines
    | parse "{_file}:{num}"
    | get num
    | into int
    | math sum
  )
}
