# Visually grep files and open them in helix
def rgv [query: string = ""] {
    let RG_PREFIX = "rg --column --line-number --no-heading --color=always --smart-case "
    (
      fzf --ansi --disabled --query ($query)
        --bind $"start:reload:($RG_PREFIX) {q}"
        --bind $"change:reload:($RG_PREFIX) {q}"
        --delimiter :
        --preview 'bat --theme "Visual Studio Dark+" --color=always {1} --highlight-line {2}'
        --preview-window 'up,60%,border-bottom,+{2}+3/3,~3'
        --bind 'enter:become(hx {1}:{2}:{3})'
    )
}

