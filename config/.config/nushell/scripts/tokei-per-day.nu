let loc = tokei -ojson | from json | get total.code

let contributors = git log --format='%aN' | lines | uniq | length

let timespan =  (date now) - (git log --reverse --format="%ad" | lines | first | into datetime) | into duration

let days = ($timespan | into int) / 8.64e13

let loc_per_contributor_per_day = ($loc / $contributors) / $days

print $"LOC per contributor per day: ($loc_per_contributor_per_day)"
