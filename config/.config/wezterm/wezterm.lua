-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This will hold the configuration.
local config = wezterm.config_builder()

config.color_scheme = 'Vs Code Dark+ (Gogh)'
config.hide_tab_bar_if_only_one_tab = true
-- config.window_background_opacity = 0.9
-- config.window_decorations = "NONE"

-- Tab bar
config.hide_tab_bar_if_only_one_tab = true
config.tab_bar_at_bottom = true
config.use_fancy_tab_bar = false

config.font = wezterm.font('ComicCode Nerd Font') -- Popular coding font
config.default_prog = { "toolbox", "run", "-c", "paru", "nu"}


-- Additional GNOME/Wayland friendly settings
-- config.front_end = "WebGpu" -- Better performance on Wayland
-- config.enable_wayland = true


config.keys = {
    {
      key = 'J',
      mods = 'CTRL|SHIFT',
      action = wezterm.action.ActivateTabRelative(1),
    },
    -- Switch to the previous tab with Ctrl+Shift+K
    {
      key = 'K',
      mods = 'CTRL|SHIFT',
      action = wezterm.action.ActivateTabRelative(-1),
    },
}

return config
